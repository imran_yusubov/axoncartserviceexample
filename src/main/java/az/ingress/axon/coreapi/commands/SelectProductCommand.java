package az.ingress.axon.coreapi.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SelectProductCommand {

    @TargetAggregateIdentifier
    private UUID foodCartId;

    private UUID productId;
    private Integer quantity;
}
