package az.ingress.axon.coreapi.events;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class FoodCartCreatedEvent {

    private UUID foodCartId;
}
