package az.ingress.axon.query;

import az.ingress.axon.coreapi.events.FoodCartCreatedEvent;
import az.ingress.axon.coreapi.queries.FindFoodCartQuery;
import az.ingress.axon.coreapi.domain.FoodCart;
import az.ingress.axon.coreapi.repository.FoodCartRepository;
import az.ingress.axon.dto.FoodCartView;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class FoodCartProjector {

    private final FoodCartRepository repository;
    private final ModelMapper mapper;

    @EventHandler
    public void on(FoodCartCreatedEvent event) {
        FoodCart foodCart = new FoodCart(event.getFoodCartId(), Collections.emptyMap());
        repository.save(foodCart);
    }

    @QueryHandler
    @Transactional
    public FoodCartView handle(FindFoodCartQuery query) {
        FoodCart foodCart = repository.findById(query.getFoodCartId()).orElse(null);
        return mapper.map(foodCart, FoodCartView.class);
    }

}
