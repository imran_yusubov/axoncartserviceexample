package az.ingress.axon.aggregates;

import az.ingress.axon.coreapi.commands.CreateFoodCartCommand;
import az.ingress.axon.coreapi.commands.DeSelectProductCommand;
import az.ingress.axon.coreapi.commands.SelectProductCommand;
import az.ingress.axon.coreapi.events.FoodCartCreatedEvent;
import az.ingress.axon.coreapi.events.ProductDeSelectedEvent;
import az.ingress.axon.coreapi.events.ProductSelectedEvent;
import az.ingress.axon.coreapi.exception.ProductDeSelectedException;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Aggregate
@NoArgsConstructor
public class FoodCartAggregate {

    @AggregateIdentifier
    private UUID foodCartId;
    private Map<UUID, Integer> selectedProducts;

    @CommandHandler
    public FoodCartAggregate(CreateFoodCartCommand command) {
        UUID aggregateId = UUID.randomUUID();
        AggregateLifecycle.apply(new FoodCartCreatedEvent(aggregateId));
    }

    @CommandHandler
    public void handle(SelectProductCommand command) {
        AggregateLifecycle.apply(ProductSelectedEvent.builder()
                .foodCartId(foodCartId)
                .productId(command.getProductId())
                .quantity(command.getQuantity())
                .build());
    }

    @CommandHandler
    public void handle(DeSelectProductCommand command) {
        AggregateLifecycle.apply(DeSelectProductCommand.builder()
                .foodCartId(foodCartId)
                .productId(command.getProductId())
                .quantity(command.getQuantity())
                .build());
    }

    @EventSourcingHandler
    public void on(FoodCartCreatedEvent foodCartCreatedEvent) {
        foodCartId = foodCartCreatedEvent.getFoodCartId();
        selectedProducts = new HashMap<>();
    }

    @EventSourcingHandler
    public void on(ProductSelectedEvent event) {
        selectedProducts.merge(event.getProductId(), event.getQuantity(), Integer::sum);
    }

    @EventSourcingHandler
    public void on(ProductDeSelectedEvent event) {
        if (!selectedProducts.containsKey(event.getProductId())) {
            throw new ProductDeSelectedException();
        }

        AggregateLifecycle.apply(ProductDeSelectedEvent.builder()
                .foodCartId(foodCartId)
                .productId(event.getProductId())
                .quantity(event.getQuantity())
                .build());
    }


}
